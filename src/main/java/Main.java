import com.thoughtworks.gameoflife.Cell;
import com.thoughtworks.gameoflife.Coordinate;
import com.thoughtworks.gameoflife.GameOfLife;

import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        ArrayList<Cell> seed = getSeed();
        GameOfLife gameOfLife = new GameOfLife(seed);
        System.out.println(gameOfLife.getState());
        gameOfLife.transitionToNextGeneration();
        System.out.println(gameOfLife.getState());
        gameOfLife.transitionToNextGeneration();
        System.out.println(gameOfLife.getState());
    }

    private static ArrayList<Cell> getSeed() {
        Cell cell1 = new Cell(new Coordinate(1, 1), true);
        Cell cell2 = new Cell(new Coordinate(1, 0), true);
        Cell cell3 = new Cell(new Coordinate(1, 2), true);
        return new ArrayList<>(Arrays.asList(cell1, cell2, cell3));
    }
}
