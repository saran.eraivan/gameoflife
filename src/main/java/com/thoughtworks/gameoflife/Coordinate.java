package com.thoughtworks.gameoflife;

import java.util.Objects;

public class Coordinate {
    private final int xPosition;
    private final int yPosition;

    public Coordinate(int xPosition, int yPosition) {
        this.xPosition = xPosition;
        this.yPosition = yPosition;
    }

    public int xDifference(Coordinate coordinate) {
        return this.xPosition - coordinate.xPosition;
    }

    public int yDifference(Coordinate coordinate) {
        return this.yPosition - coordinate.yPosition;
    }

    public Coordinate incrementX() {
        return new Coordinate(xPosition + 1, yPosition);
    }

    public Coordinate decrementX() {
        return new Coordinate(xPosition - 1, yPosition);
    }

    public Coordinate incrementY() {
        return new Coordinate(xPosition, yPosition + 1);
    }

    public Coordinate decrementY() {
        return new Coordinate(xPosition, yPosition - 1);
    }

    public boolean isNeighboring(Coordinate other) {
        boolean areXValuesNear = Math.abs(this.xPosition - other.xPosition) < 2;
        boolean areYValuesNear = Math.abs(this.yPosition - other.yPosition) < 2;
        return areXValuesNear && areYValuesNear;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coordinate that = (Coordinate) o;
        return xPosition == that.xPosition && yPosition == that.yPosition;
    }

    @Override
    public int hashCode() {
        return Objects.hash(xPosition, yPosition);
    }

    @Override
    public String toString() {
        return "(" + xPosition + "," + yPosition + ')';
    }

}
