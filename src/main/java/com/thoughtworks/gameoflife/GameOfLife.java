package com.thoughtworks.gameoflife;

import java.util.ArrayList;
import java.util.HashSet;

public class GameOfLife {
    private ArrayList<Cell> grid;

    public GameOfLife(ArrayList<Cell> seed) {
        updateGridWithLivingAndDeadFrom(seed);
    }

    public ArrayList<Cell> getState() {
        ArrayList<Cell> livingCells = new ArrayList<>();
        for (Cell cell : grid) {
            if (cell.isLiving()) livingCells.add(cell);
        }
        livingCells.sort(Cell.CellComparator);
        return livingCells;
    }

    public void transitionToNextGeneration() {
        ArrayList<Cell> nextGeneration = new ArrayList<>();
        for (Cell cell : grid) {
            int livingNeighborCount = getLivingNeighborCount(cell);
            Cell afterTransition = cell.transition(livingNeighborCount);
            nextGeneration.add(afterTransition);
        }
        updateGridWithLivingAndDeadFrom(nextGeneration);
    }

    private int getLivingNeighborCount(Cell givenCell) {
        int neighborCount = 0;
        ArrayList<Cell> livingCells = getState();
        for (Cell livingCell : livingCells) {
            if (givenCell.isNeighborTo(livingCell)) neighborCount++;
        }
        return neighborCount;
    }

    private void updateGridWithLivingAndDeadFrom(ArrayList<Cell> seed) {
        seed.removeIf(cell -> !(cell.isLiving()));
        grid = new ArrayList<>(seed);
        ArrayList<Cell> deadNeighbors = findDeadNeighbors(seed);
        grid.addAll(deadNeighbors);
    }

    private ArrayList<Cell> findDeadNeighbors(ArrayList<Cell> livingCells) {
        HashSet<Cell> deadNeighbors = new HashSet<>();
        for (Cell cell : livingCells) {
            ArrayList<Cell> NeighborsOfTheCell = cell.neighbors();
            NeighborsOfTheCell.removeIf(livingCells::contains);
            deadNeighbors.addAll(NeighborsOfTheCell);
        }
        return new ArrayList<>(deadNeighbors);
    }
}
