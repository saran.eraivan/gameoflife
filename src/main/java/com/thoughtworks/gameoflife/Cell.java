package com.thoughtworks.gameoflife;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Objects;

public class Cell {
    private final Coordinate coordinate;
    private final boolean isLiving;

    public Cell(Coordinate coordinate, boolean isLiving) {
        this.coordinate = coordinate;
        this.isLiving = isLiving;
    }

    public boolean isLiving() {
        return this.isLiving;
    }

    public ArrayList<Cell> neighbors() {
        ArrayList<Cell> neighbors = new ArrayList<>();
        getLeftAndRightNeighbors(neighbors, this);

        Cell topNeighbor = this.topCell();
        neighbors.add(topNeighbor);
        getLeftAndRightNeighbors(neighbors, topNeighbor);

        Cell bottomNeighbor = this.bottomCell();
        neighbors.add(bottomNeighbor);
        getLeftAndRightNeighbors(neighbors, bottomNeighbor);
        return neighbors;
    }

    public Cell transition(int livingNeighborCount) {
        switch (livingNeighborCount) {
            case 3:
                return new Cell(this.coordinate, true);
            case 2:
                if (this.isLiving) return new Cell(this.coordinate, true);
            default:
                return new Cell(this.coordinate, false);
        }
    }

    public boolean isNeighborTo(Cell other) {
        if (!(this.equals(other))) {
            return this.coordinate.isNeighboring(other.coordinate);
        }
        return false;
    }

    public static Comparator<Cell> CellComparator = (cell1, cell2) -> {
        int xDifference = cell1.coordinate.xDifference(cell2.coordinate);
        if (xDifference == 0) {
            return cell1.coordinate.yDifference(cell2.coordinate);
        }
        return xDifference;
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cell cell = (Cell) o;
        return coordinate.equals(cell.coordinate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(coordinate);
    }

    @Override
    public String toString() {
        return (coordinate + ": " + isLiving);
    }

    private void getLeftAndRightNeighbors(ArrayList<Cell> neighbors, Cell cell) {
        neighbors.add(cell.leftCell());
        neighbors.add(cell.rightCell());
    }

    private Cell topCell() {
        return new Cell(this.coordinate.decrementX(), false);
    }

    private Cell bottomCell() {
        return new Cell(this.coordinate.incrementX(), false);
    }

    private Cell leftCell() {
        return new Cell(this.coordinate.decrementY(), false);
    }

    private Cell rightCell() {
        return new Cell(this.coordinate.incrementY(), false);
    }
}
