package com.thoughtworks.gameoflife;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CoordinateTest {
    @Test
    void shouldReturnTheXAndYDifferenceOfTwoCoordinates() {
        Coordinate coordinate3And5 = new Coordinate(3, 5);
        Coordinate coordinate4And1 = new Coordinate(4, 1);

        assertEquals(-1, coordinate3And5.xDifference(coordinate4And1));
        assertEquals(4, coordinate3And5.yDifference(coordinate4And1));
    }

    @Test
    void shouldIncrementXvalue() {
        Coordinate coordinate3And5 = new Coordinate(3, 5);
        Coordinate coordinate4And5 = new Coordinate(4, 5);

        assertEquals(coordinate3And5.incrementX(), coordinate4And5);
    }

    @Test
    void shouldDecrementXvalue() {
        Coordinate coordinate3And5 = new Coordinate(3, 5);
        Coordinate coordinate2And5 = new Coordinate(2, 5);

        coordinate3And5 = coordinate3And5.decrementX();

        assertEquals(coordinate3And5, coordinate2And5);
    }

    @Test
    void shouldIncrementYValue() {
        Coordinate coordinate3And5 = new Coordinate(3, 5);
        Coordinate coordinate3And6 = new Coordinate(3, 6);

        coordinate3And5 = coordinate3And5.incrementY();

        assertEquals(coordinate3And5, coordinate3And6);
    }

    @Test
    void shouldDecrementYValue() {
        Coordinate coordinate3And5 = new Coordinate(3, 5);
        Coordinate coordinate3And4 = new Coordinate(3, 4);

        coordinate3And5 = coordinate3And5.decrementY();

        assertEquals(coordinate3And5, coordinate3And4);
    }

    @Test
    void shouldBeNeighborsWhenCoordinatesDifferByOneStepOnly() {
        Coordinate coordinate1And1 = new Coordinate(1, 1);
        Coordinate coordinate0And0 = new Coordinate(0, 0);
        Coordinate coordinate2And3 = new Coordinate(2, 3);

        assertTrue(coordinate1And1.isNeighboring(coordinate0And0));
        assertFalse(coordinate1And1.isNeighboring(coordinate2And3));
    }

    @Test
    void coordinateShouldBeEqualWhenHavingSameCoordinates() {
        Coordinate coordinate1And1 = new Coordinate(1, 1);
        Coordinate coordinate1And1Again = new Coordinate(1, 1);
        Coordinate coordinate2And1 = new Coordinate(2, 1);

        assertEquals(coordinate1And1, coordinate1And1Again);
        assertNotEquals(coordinate1And1, coordinate2And1);
    }

    @Test
    void coordinateShouldBeEqualIfBothAreSameObject() {
        Coordinate coordinate1And1 = new Coordinate(1, 1);

        assertEquals(coordinate1And1, coordinate1And1);
    }

    @Test
    void coordinateShouldNotBeEqualIfDifferentClasses() {
        Coordinate coordinate1And1 = new Coordinate(1, 1);
        Cell cell = new Cell(coordinate1And1, true);

        assertNotEquals(cell, coordinate1And1);
    }

    @Test
    void coordinateShouldNotBeEqualToNull() {
        Coordinate coordinate1And1 = new Coordinate(1, 1);

        assertNotEquals(null, coordinate1And1);
    }

    @Test
    void hashShouldBeEqualForSameCoordinates() {
        Coordinate coordinate1And2 = new Coordinate(1, 2);
        Coordinate coordinate1And2Again = new Coordinate(1, 2);
        Coordinate coordinate2And2 = new Coordinate(2, 2);

        assertEquals(coordinate1And2.hashCode(), coordinate1And2Again.hashCode());
        assertNotEquals(coordinate1And2.hashCode(), coordinate2And2.hashCode());
    }

    @Test
    void shouldPrintAsXAndYInParenthesis() {
        Coordinate coordinate1And1 = new Coordinate(1, 1);

        assertEquals("(1,1)", coordinate1And1.toString());
    }
}
