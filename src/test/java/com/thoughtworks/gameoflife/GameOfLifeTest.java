package com.thoughtworks.gameoflife;

import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

public class GameOfLifeTest {
    @Test
    void shouldReturnTheLivingCellsOfTheGrid() {
        int[][] inputCellPositions = new int[][]{{1, 0}, {1, 1}, {1, 2}};
        ArrayList<Cell> seed = getLivingCellsFromXAndYValues(inputCellPositions);

        GameOfLife gameOfLife = new GameOfLife(seed);

        assertEquals(seed, gameOfLife.getState());
    }


    @Test
    void shouldNotTransitionForBoatPattern() {
        int[][] inputCellPositions = new int[][]{{1, 1}, {1, 2}, {2, 1}, {2, 2}};
        ArrayList<Cell> seed = getLivingCellsFromXAndYValues(inputCellPositions);
        GameOfLife gameOfLife = new GameOfLife(seed);

        gameOfLife.transitionToNextGeneration();

        assertEquals(seed, gameOfLife.getState());
    }

    @Test
    void shouldOscillateForBlinkerPattern() {
        int[][] inputCellPositions = new int[][]{{1, 1}, {1, 0}, {1, 2}};
        ArrayList<Cell> seed = getLivingCellsFromXAndYValues(inputCellPositions);
        GameOfLife gameOfLife = new GameOfLife(seed);

        int[][] expectedCellPositions = new int[][]{{0, 1}, {1, 1}, {2, 1}};
        ArrayList<Cell> expectedLivingCells = getLivingCellsFromXAndYValues(expectedCellPositions);

        gameOfLife.transitionToNextGeneration();

        assertEquals(expectedLivingCells, gameOfLife.getState());
    }

    @Test
    void shouldOscillateForToadPattern() {
        int[][] inputCellPositions = new int[][]{{1, 1}, {1, 2}, {1, 3}, {2, 2}, {2, 3}, {2, 4}};
        ArrayList<Cell> seed = getLivingCellsFromXAndYValues(inputCellPositions);
        GameOfLife gameOfLife = new GameOfLife(seed);

        int[][] expectedCellPositions = new int[][]{{0, 2}, {1, 1}, {1, 4}, {2, 1}, {2, 4}, {3, 3}};
        ArrayList<Cell> expectedLivingCells = getLivingCellsFromXAndYValues(expectedCellPositions);

        gameOfLife.transitionToNextGeneration();

        assertEquals(expectedLivingCells, gameOfLife.getState());
    }

    private ArrayList<Cell> getLivingCellsFromXAndYValues(int[][] inputXAndYValues) {
        ArrayList<Cell> cellsFromInputValues = new ArrayList<>();
        for (int[] xAndY : inputXAndYValues) {
            Cell livingCell = new Cell(new Coordinate(xAndY[0], xAndY[1]), true);
            cellsFromInputValues.add(livingCell);
        }
        return cellsFromInputValues;
    }
}
