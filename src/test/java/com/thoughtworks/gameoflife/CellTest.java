package com.thoughtworks.gameoflife;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class CellTest {
    @Test
    void shouldReturnTrueWhenTheCellIsLiving() {
        Cell cellWithX1AndY1 = new Cell(new Coordinate(1, 1), true);
        Cell cellWithX1AndY2 = new Cell(new Coordinate(1, 2), false);

        assertTrue(cellWithX1AndY1.isLiving());
        assertFalse(cellWithX1AndY2.isLiving());
    }

    @Test
    void shouldBeANeighborWhenCoordinatesAreNear() {
        Cell cellWithX1AndY1 = new Cell(new Coordinate(1, 1), true);
        Cell cellWithX0AndY0 = new Cell(new Coordinate(0, 0), false);
        Cell cellWithX3AndY3 = new Cell(new Coordinate(3, 3), true);

        assertTrue(cellWithX1AndY1.isNeighborTo(cellWithX0AndY0));
        assertFalse(cellWithX1AndY1.isNeighborTo(cellWithX3AndY3));
    }

    @Test
    void shouldReturnTheNeighbors() {
        Cell cellWithX1AndY1 = new Cell(new Coordinate(1, 1), true);
        Cell cellWithX0AndY0 = new Cell(new Coordinate(0, 0), false);
        Cell cellWithX3AndY3 = new Cell(new Coordinate(3, 3), false);

        ArrayList<Cell> neighbors = cellWithX1AndY1.neighbors();

        assertEquals(8, neighbors.size());
        assertTrue(neighbors.contains(cellWithX0AndY0));
        assertFalse(neighbors.contains(cellWithX3AndY3));
    }

    @Test
    void livingCellShouldTransitionBasedOnItsNeighborCount() {
        Cell cell = new Cell(new Coordinate(0, 1), true);

        Cell transitionWith3Neighbors = cell.transition(3);
        Cell transitionWith2Neighbors = cell.transition(2);
        Cell transitionWith1Neighbor = cell.transition(1);

        assertTrue(transitionWith3Neighbors.isLiving());
        assertTrue(transitionWith2Neighbors.isLiving());
        assertFalse(transitionWith1Neighbor.isLiving());
    }

    @Test
    void deadCellShouldLiveOnlyIfThreeLivingNeighbors() {
        Cell cell = new Cell(new Coordinate(1, 0), false);

        Cell transitionWith1Neighbor = cell.transition(1);
        Cell transitionWith3Neighbors = cell.transition(3);

        assertFalse(transitionWith1Neighbor.isLiving());
        assertTrue(transitionWith3Neighbors.isLiving());
    }

    @Test
    void shouldSortAccordingToTheirCoordinates() {
        Cell cellWithX0AndY1 = new Cell(new Coordinate(0, 1), true);
        Cell cellWithX0AndY2 = new Cell(new Coordinate(0, 2), true);
        Cell cellWithX1AndY0 = new Cell(new Coordinate(1, 0), true);
        ArrayList<Cell> seed = new ArrayList<>(Arrays.asList(cellWithX1AndY0, cellWithX0AndY1, cellWithX0AndY2));
        ArrayList<Cell> expected = new ArrayList<>(Arrays.asList(cellWithX0AndY1, cellWithX0AndY2, cellWithX1AndY0));

        seed.sort(Cell.CellComparator);

        assertEquals(expected, seed);
    }

    @Test
    void shouldBeEqualWhenHavingSameCoordinates() {
        Cell cellWithX0AndY1 = new Cell(new Coordinate(0, 1), true);
        Cell cellWithX0AndY2 = new Cell(new Coordinate(0, 2), true);
        Cell cellWithX0AndY2Again = new Cell(new Coordinate(0, 2), true);

        assertNotEquals(cellWithX0AndY1, cellWithX0AndY2);
        assertEquals(cellWithX0AndY2, cellWithX0AndY2Again);
    }

    @Test
    void shouldBeEqualWhenBothAreSameObjects() {
        Cell cell = new Cell(new Coordinate(1, 1), true);

        assertEquals(cell, cell);
    }

    @Test
    void shouldNotBeEqualWhenNullOrOfDifferentClass() {
        Cell cell = new Cell(new Coordinate(1, 1), true);
        Coordinate coordinate = new Coordinate(1, 1);

        assertNotEquals(null, cell);
        assertNotEquals(coordinate, cell);
    }

    @Test
    void shouldHaveHashcodeBasedOnCoordinate() {
        Cell cell1 = new Cell(new Coordinate(1, 1), true);
        Cell cell2 = new Cell(new Coordinate(1, 1), false);

        assertEquals(cell1.hashCode(), cell2.hashCode());
    }

    @Test
    void shouldBePrintedAsAStringOfCoordinateAndState() {
        Cell cell1 = new Cell(new Coordinate(0, 1), true);

        assertEquals("(0,1): true", cell1.toString());
    }
}
